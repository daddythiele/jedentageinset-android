Jedentageinset Android
======================

[![](https://gitlab.com/derSchabi/jedentageinset-android/-/raw/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)](https://jedentageinset.de)

Das ist die kleine aber feine Android App für
[jedentageinset.de](https://jedentageinset.de).
Die App kann als Companion für [NewPipe](https://newpipe.net) oder der SoundCloud app verwendet werden.
Wenn du Verbesserungsvorschläge oder Bugs gefunden hast, immer her mit den Pullrequests oder Issues ;)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.schabi.jedentageinset/)

## License
[![GNU GPLv3 Image](https://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl-3.0.en.html)  

NewPipe is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. 
