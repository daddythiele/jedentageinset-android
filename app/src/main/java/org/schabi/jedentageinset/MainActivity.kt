package org.schabi.jedentageinset

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.api.load
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers


class MainActivity : AppCompatActivity() {

    val TAG = "MainAcitivty"

    val setItems = mutableListOf<Crawler.SetItem>()
    var currentPage = 1
    private val setAdapter = SetAdapter(this, setItems)
    lateinit var loadingLayout : SwipeRefreshLayout
    val disposableList = mutableListOf<Disposable>()
    var requestsLocked = false; // if true new requests are deniled


    private class SetAdapter(context : Context, setItems: List<Crawler.SetItem>) : RecyclerView.Adapter<SetAdapter.SetViewHolder>() {

        val setItems = setItems

        class SetViewHolder(setItem : View) : RecyclerView.ViewHolder(setItem) {
            val titleImage = itemView.findViewById<ImageView>(R.id.set_tittle_image)
            val titleTextView = itemView.findViewById<AppCompatTextView>(R.id.title_text_view)
            val contentTextView = itemView.findViewById<AppCompatTextView>(R.id.description_text_view)
            val setItemCardView = itemView.findViewById<CardView>(R.id.set_item_card_view)
        }

        val context = context

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetViewHolder {
            val setItem = LayoutInflater.from(parent.context)
                .inflate(R.layout.set_item, parent, false)

            val ocsLight =  ResourcesCompat.getFont(parent.context, R.font.osc_light)
            val sspRegular = ResourcesCompat.getFont(parent.context, R.font.ssp_regular)
            val holder = SetViewHolder(setItem)
            holder.contentTextView.typeface = sspRegular
            holder.titleTextView.typeface = ocsLight
            return holder
        }

        override fun onBindViewHolder(holder: SetViewHolder, position: Int) {
            val item = setItems[position]
            holder.titleTextView.setText(item.title)
            holder.contentTextView.setText(item.description)
            holder.titleImage.load(item.imgUrl) {
                crossfade(500)
                placeholder(R.drawable.img_placeholder)
            }
            holder.setItemCardView.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra(DetailActivity.EXTRA_LINK, item.link)
                context.startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return setItems.size
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val displayMetrics = resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density

        val gridLayoutColumns = if ((dpWidth - 10) / 320 < 1 ) {
            1
        } else {
            ((dpWidth - 10) / 320).toInt()
        }


        println("------------------------" + gridLayoutColumns + "---" + dpWidth)

        loadingLayout = findViewById<SwipeRefreshLayout>(R.id.refresh_sets_view)
        val setLayoutManager = GridLayoutManager(this, gridLayoutColumns)

        val recyclerView = findViewById<RecyclerView>(R.id.sets_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = setLayoutManager
            adapter = setAdapter
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    loadNextPage()
                }
            }
        })

        refreshAll()

        loadingLayout.setOnRefreshListener {
            refreshAll()
        }

        findViewById<ImageView>(R.id.jtesButton).setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse("https://jedentageinset.de")
            startActivity(i)
        }
    }

    @SuppressLint("CheckResult")
    fun refreshAll() {
        if(requestsLocked)
            return;
        disposableList.forEach { disposable ->
            disposable.dispose()
        }
        setItems.clear()
        setAdapter.notifyDataSetChanged()
        currentPage = 1
        load(currentPage)
    }

    fun loadNextPage() {
        if(requestsLocked)
            return;
        currentPage += 1
        load(currentPage)
    }

    fun load(page: Int) {
        requestsLocked = true
        loadingLayout.isRefreshing = true
        disposableList.add(
            Crawler().getItems(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ newItems ->
                    setItems.addAll(newItems)
                    loadingLayout.isRefreshing = false
                    setAdapter.notifyDataSetChanged()
                    requestsLocked = false
                }, { throwable ->
                    val toast = Utils.getCustomToast(this@MainActivity,
                        throwable.toString() + ": " + throwable.message,
                        Toast.LENGTH_SHORT)
                    toast.show()
                    throwable.printStackTrace()
                    loadingLayout.isRefreshing = false
                    requestsLocked = false
                }))
    }
}
