package org.schabi.jedentageinset

import android.content.Context
import android.widget.TextView
import android.widget.Toast

object Utils {
    fun getCustomToast(context: Context, message: String, duration: Int): Toast {
        val toast = Toast.makeText(context, message, duration)
        val textView = toast.view?.findViewById<TextView>(android.R.id.message)
        textView?.setBackgroundColor(context.resources.getColor(android.R.color.transparent))
        return toast
    }
}