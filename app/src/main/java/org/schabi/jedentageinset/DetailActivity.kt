package org.schabi.jedentageinset

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.graphics.drawable.DrawableWrapper
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.LevelListDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Contacts
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import coil.Coil
import coil.api.get
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class DetailActivity : AppCompatActivity() {


    companion object {
        const val EXTRA_LINK = "EXTRA_LINK"
    }

    class SimpleImageGetter(context: Context, contentView : TextView) : Html.ImageGetter {
        val context = context
        val contentView = contentView

        override fun getDrawable(source: String?): Drawable? {
            val placeHolder = ResourcesCompat.getDrawable(context.resources, R.drawable.logo_white, null)!!
            val wrapper = LevelListDrawable()
            wrapper.setBounds(0, 0, placeHolder.intrinsicWidth/3, placeHolder.intrinsicHeight/3)
            wrapper.addLevel(0, 0, placeHolder)
            GlobalScope.launch {
                val image = Coil.get(source!!)
                GlobalScope.launch(Dispatchers.Main) {
                    wrapper.addLevel(1, 1, image)
                    wrapper.setBounds(0, 0, image.intrinsicWidth, image.intrinsicHeight)
                    wrapper.level = 1
                    val text = contentView.text
                    contentView.text = text
                }
            }

            return wrapper
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(findViewById(R.id.toolbar))

        val link = intent.getStringExtra(EXTRA_LINK)!!

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val scrollView = findViewById<ScrollView>(R.id.detail_scroll_view)
        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        val contentView = findViewById<TextView>(R.id.content_view)
        contentView.typeface =  ResourcesCompat.getFont(this, R.font.ssp_regular)

        findViewById<ImageView>(R.id.jtesButton).setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(link)
            startActivity(i)
        }

        Crawler().getContent(link)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ setInfo ->
                val html = setInfo.site
                contentView.text = HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_LEGACY, SimpleImageGetter(this, contentView), null)
                contentView.movementMethod = LinkMovementMethod.getInstance()
                progressBar.visibility = View.GONE
                scrollView.visibility = View.VISIBLE
                fab.visibility = View.VISIBLE
                fab.setOnClickListener {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(setInfo.streamLink)
                    startActivity(i)
                }
            }, { throwable ->
                Utils.getCustomToast(this@DetailActivity, throwable.toString() + ": " + throwable.message, Toast.LENGTH_SHORT)
                throwable.printStackTrace()
                progressBar.visibility = View.GONE
            })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
