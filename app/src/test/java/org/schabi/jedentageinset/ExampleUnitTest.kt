package org.schabi.jedentageinset

import org.jsoup.Jsoup
import org.junit.Test

import org.junit.Assert.*
import org.junit.BeforeClass

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

class CrawlerTest {
    @Test
    fun testRequestMainSide() {
        Crawler().getItems(1)
            .subscribe({ itemList ->
                assertTrue(itemList.size == 6)
                for( item in itemList) {
                    assertTrue(item.link.startsWith("https://"))
                    assertTrue(item.imgUrl.startsWith("https://"))
                    assertFalse(item.description.isEmpty())
                    assertFalse(item.title.isEmpty())
                }
            }, {throwable ->
                throwable.printStackTrace()
            })
    }

    @Test
    fun testRequestMainSidePageTwo() {
        Crawler().getItems(2)
            .subscribe({ itemList ->
                assertTrue(itemList.size == 6)
                for( item in itemList) {
                    assertTrue(item.link.startsWith("https://"))
                    assertTrue(item.imgUrl.startsWith("https://"))
                    assertFalse(item.description.isEmpty())
                    assertFalse(item.title.isEmpty())
                }
            }, {throwable ->
                throwable.printStackTrace()
            })
    }

    @Test
    fun testGetContent() {
        Crawler().getContent("https://www.jedentageinset.de/2020/03/17/2736-kollektiv-ost-radio-butzke-radio-fritz-21-02-2020/")
            .subscribe { setItem ->
                val html = setItem.site
                assertFalse(html.isEmpty())
                Jsoup.parse(html)
                assertTrue(setItem.streamLink.startsWith("https://soundcloud.com/"))
            }
    }
}
